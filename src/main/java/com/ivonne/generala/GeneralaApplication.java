package com.ivonne.generala;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeneralaApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeneralaApplication.class, args);
	}

}
