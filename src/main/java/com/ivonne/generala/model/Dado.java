package com.ivonne.generala.model;

import java.util.ArrayList;
import java.util.Date;

public class Dado {

    private ArrayList<Integer> dados;
    private String jugada;
    private Date tstamp;

    public Dado(ArrayList<Integer> dados, String jugada, Date tstamp) {
        this.dados = dados;
        this.jugada = jugada;
        this.tstamp = tstamp;
    }

    public Dado() {

    }

    public ArrayList<Integer> getDados() {
        return dados;
    }

    public void setDados(ArrayList<Integer> dados) {
        this.dados = dados;
    }

    public String getJugada() {
        return jugada;
    }

    public void setJugada(String jugada) {
        this.jugada = jugada;
    }

    public Date getTstamp() {
        return tstamp;
    }

    public void setTstamp(Date tstamp) {
        this.tstamp = tstamp;
    }

    public String obtenerJugada(ArrayList<Integer> dados){
        int elem2 = 0;
        int iguales1 = 1;
        int iguales2 = 1;
        boolean posibleEscalera = false;
        boolean escalera=false;

        int elem1 = dados.get(0);

        for(int pos = 1; pos<dados.size(); ++pos){

            if(!posibleEscalera){
                if(elem1 == dados.get(pos)){
                    ++iguales1;
                } else if(elem2 == dados.get(pos)){
                    ++iguales2;
                } else if (elem2 == 0) {
                    elem2 = dados.get(pos);
                } else {
                    if(((elem1 + 1) == elem2) || ((elem1 == 6) && (elem2 == 1))){
                        posibleEscalera = true;
                    }
                }

            }

            if(posibleEscalera){
                if(!((dados.get(--pos)+1 == dados.get(pos)) || (dados.get(--pos) == 6 && dados.get(pos)==1))){
                    escalera = false;
                    break;
                }
            }
        }

        if(iguales1 == dados.size()) return "GENERALA";
        else if(iguales1 == dados.size()-1 || iguales2 == dados.size()-1) return "POKER";
        else if(iguales1 == 3 && iguales2 == 2 || iguales2 == 3 && iguales1 == 2) return "FULL";
        else if(escalera) return "ESCALERA";

        return "NADA";
    }
}
