package com.ivonne.generala.controlador;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<MensajeError> handleUncaughtException(Exception ex){
        MensajeError exceptionResponse = new MensajeError("g100","Error Interno del Servidor");

        return new ResponseEntity<>(exceptionResponse,new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}

class MensajeError {
    private String codigo;
    private String mensaje;

    public MensajeError(String codigo, String mensaje) {
        this.codigo = codigo;
        this.mensaje = mensaje;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje (String mensaje){
        this.mensaje = mensaje;
    }

}
