package com.ivonne.generala.controlador;

import com.ivonne.generala.model.Dado;
import com.ivonne.generala.servicio.ServicioDado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class controladorDado {

    @Autowired
    ServicioDado servicioDado;

    @CrossOrigin
    @GetMapping(path = "/lanzar")
    public Dado lanzar() {
        Dado dado = servicioDado.lanzar();
        if(dado == null){
            throw new DadoModelNotFoundException("No hay nada que retornar");
        }
        return dado;
    }

}
