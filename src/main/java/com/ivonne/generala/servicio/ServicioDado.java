package com.ivonne.generala.servicio;

import com.ivonne.generala.model.Dado;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;

@Service
public class ServicioDado {

    public Dado lanzar() {

        Dado dado = new Dado();
        ArrayList<Integer> dados = new ArrayList<>(5);

        for(int i=0; i<5; i++){
            dados.add((int) Math.floor(Math.random() * (7 - 1) + 1));
        }
        String jugada = dado.obtenerJugada(dados);
        Date fecha = new Date();

        return new Dado(dados,jugada,fecha);
    }


}
